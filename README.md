# Auto Auto-Rotate

Auto Auto-Rotate is a simple app learns for which apps auto-rotate should
be set and switches automatically.

I personally don't like auto-rotate, so it's always switched off unless I
am taking a look at pictures, watching a movie or using the GPS.

With this app, I don't need to worry about setting it every time :)
