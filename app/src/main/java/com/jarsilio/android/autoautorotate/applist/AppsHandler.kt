package com.jarsilio.android.autoautorotate.applist

import android.content.Context
import android.content.Intent
import android.content.pm.ApplicationInfo
import android.content.pm.PackageManager
import android.content.pm.ResolveInfo
import com.jarsilio.android.autoautorotate.R
import timber.log.Timber

class AppsHandler(context: Context) {
    private val applicationContext: Context = context.applicationContext

    private val appsDao = AppDatabase.getInstance(applicationContext).appsDao()

    fun updateAppsDatabase() {
        Thread {
            addNewAppsToDatabase()
            removeObsoleteAppsFromDatabase()
        }.start()
    }

    fun isAutorotate(packageName: String): Boolean {
        val app = appsDao.loadByPackageName(packageName)
        return app?.isAutorotate ?: false
    }

    private fun addNewAppsToDatabase() {
        Timber.d("Adding new apps to database")
        val intent = Intent(Intent.ACTION_MAIN, null)
        intent.addCategory(Intent.CATEGORY_LAUNCHER)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED
        val resolveInfoList = applicationContext.packageManager.queryIntentActivities(intent, 0)
        for (resolveInfo in resolveInfoList) {
            val packageName = resolveInfo.activityInfo.applicationInfo.packageName

            val isAppAlreadyInDatabase = appsDao.loadByPackageName(packageName) != null
            if (!isAppAlreadyInDatabase) {
                val name = getAppName(packageName)
                val isSystem = isSystemPackage(resolveInfo)
                val isAutorotate = false

                val app = App(packageName, name, isSystem, isAutorotate)
                appsDao.insertIfNotExists(app) // If not exists because there might be apps that expose more than one launcher
            }
        }
    }

    private fun removeObsoleteAppsFromDatabase() {
        Timber.d("Removing obsolete apps from database (probably uninstalled)")
        for (app in appsDao.all) {
            if (!isAppInstalled(app)) {
                Timber.v("-> $app")
                appsDao.delete(app)
            }
        }
    }

    private fun isAppInstalled(app: App): Boolean {
        return try {
            applicationContext.packageManager.getApplicationInfo(app.packageName, 0)
            true
        } catch (e: PackageManager.NameNotFoundException) {
            false
        }
    }

    private fun getAppName(packageName: String): String {
        return try {
            val applicationInfo = applicationContext.packageManager.getApplicationInfo(packageName, 0)
            applicationContext.packageManager.getApplicationLabel(applicationInfo) as String
        } catch (e: PackageManager.NameNotFoundException) {
            Timber.e(e)
            applicationContext.getString(R.string.untitled_app)
        }
    }

    private fun isSystemPackage(packageName: String): Boolean {
        return applicationContext.packageManager.getApplicationInfo(packageName, 0).flags and ApplicationInfo.FLAG_SYSTEM != 0
    }

    private fun isSystemPackage(resolveInfo: ResolveInfo): Boolean {
        return resolveInfo.activityInfo.applicationInfo.flags and ApplicationInfo.FLAG_SYSTEM != 0
    }
}
