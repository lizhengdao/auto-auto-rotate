package com.jarsilio.android.autoautorotate.prefs

import android.content.Context
import android.content.SharedPreferences
import androidx.preference.CheckBoxPreference
import androidx.preference.Preference
import androidx.preference.PreferenceManager
import androidx.preference.SwitchPreference
import androidx.preference.SwitchPreferenceCompat
import com.jarsilio.android.autoautorotate.MainActivity
import com.jarsilio.android.autoautorotate.R
import com.jarsilio.android.common.utils.SingletonHolder

class Prefs private constructor(context: Context) {
    val prefs: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
    var settingsFragment: MainActivity.SettingsFragment? = null

    /* User prefs */
    val IS_ENABLED: String = context.getString(R.string.pref_enabled_key)
    val SHOW_NOTIFICATION: String = context.getString(R.string.pref_notification_key)
    val APP_LIST: String = context.getString(R.string.pref_app_list_key)

    var isEnabled: Boolean
        get() = prefs.getBoolean(IS_ENABLED, true)
        set(value) = setBooleanPreference(IS_ENABLED, value)

    var showNotification: Boolean
        get() = prefs.getBoolean(SHOW_NOTIFICATION, true)
        set(value) = setBooleanPreference(SHOW_NOTIFICATION, value)

    private fun setBooleanPreference(key: String, value: Boolean) {
        // This changes the GUI, but it needs the PreferencesActivity to have started
        when (val preference = settingsFragment?.findPreference(key) as Preference?) {
            is CheckBoxPreference -> preference.isChecked = value
            is SwitchPreference -> preference.isChecked = value
            is SwitchPreferenceCompat -> preference.isChecked = value
        }
        // This doesn't change the GUI
        prefs.edit().putBoolean(key, value).apply()
    }

    companion object : SingletonHolder<Prefs, Context>(::Prefs)
}
