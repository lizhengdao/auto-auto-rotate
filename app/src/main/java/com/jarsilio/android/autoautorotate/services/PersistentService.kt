package com.jarsilio.android.autoautorotate.services

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.database.ContentObserver
import android.os.Build
import android.os.Handler
import android.os.IBinder
import android.provider.Settings
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import com.jarsilio.android.autoautorotate.MainActivity
import com.jarsilio.android.autoautorotate.R
import com.jarsilio.android.autoautorotate.applist.AppDatabase
import com.jarsilio.android.autoautorotate.applist.AppsHandler
import com.jarsilio.android.autoautorotate.extensions.currentlyRunningApp
import com.jarsilio.android.autoautorotate.extensions.isIgnoringBatteryOptimizations
import com.jarsilio.android.autoautorotate.extensions.isScreenOn
import com.jarsilio.android.autoautorotate.prefs.Prefs
import timber.log.Timber

internal class AutoRotateSettingObserver(private val context: Context, handler: Handler?) : ContentObserver(handler) {

    private val appsDao = AppDatabase.getInstance(context).appsDao()
    private val appsHandler = AppsHandler(context)

    override fun onChange(selfChange: Boolean) {
        val currentlyRunningApp = context.currentlyRunningApp

        val autoRotate = Settings.System.getInt(context.contentResolver, Settings.System.ACCELEROMETER_ROTATION) == 1
        Timber.d("Auto-Rotate setting changed: $autoRotate")

        if (autoRotate) {
            if (!appsHandler.isAutorotate(currentlyRunningApp)) {
                Timber.d("Auto-rotate set to true for $currentlyRunningApp. Adding to list of apps to auto-rotate")
                appsDao.setAutorotate(currentlyRunningApp, true)
            }
        } else {
            if (appsHandler.isAutorotate(currentlyRunningApp)) {
                Timber.d("Auto-rotate set to false for $currentlyRunningApp. Removing from list of apps to auto-rotate")
                appsDao.setAutorotate(currentlyRunningApp, false)
            }
        }
    }
}

class PersistentService : Service() {

    private val autoRotateSettingObserver: AutoRotateSettingObserver by lazy { AutoRotateSettingObserver(this, null) }

    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    private fun start() {
        registerScreenReceiver()
        registerAutoRotateSettingObserver()
    }

    private fun stop() {
        unregisterReceiver(ScreenReceiver)
        unregisterAutoRotateSettingObserver()
    }

    override fun onDestroy() {
        stop()
    }

    override fun onCreate() {
        super.onCreate()
        start()
    }

    private fun registerScreenReceiver() {
        val filter = IntentFilter(Intent.ACTION_SCREEN_ON)
        filter.addAction(Intent.ACTION_SCREEN_OFF)
        registerReceiver(ScreenReceiver, filter)

        if (isScreenOn) {
            // Start directly if screen is on
            ScreenReceiver.scheduleTimer(this)
        }
    }

    private fun registerAutoRotateSettingObserver() {
        contentResolver.registerContentObserver(Settings.System.getUriFor(Settings.System.ACCELEROMETER_ROTATION), true, autoRotateSettingObserver)
    }

    private fun unregisterAutoRotateSettingObserver() {
        contentResolver.unregisterContentObserver(autoRotateSettingObserver)
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {

        if (shouldStartForegroundService(this)) {
            startForegroundService()
        }

        return START_STICKY
    }

    private fun startForegroundService() {
        Timber.d("Starting ForegroundService")

        val notificationIntent = Intent(this, MainActivity::class.java)
        notificationIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        val notificationPendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0)

        val notificationBuilder = NotificationCompat.Builder(this, "persistent")
                .setContentText(getString(R.string.notification_tap_to_open))
                .setShowWhen(false)
                .setContentIntent(notificationPendingIntent)
                .setColor(ContextCompat.getColor(this, R.color.colorPrimary))
                .setSmallIcon(R.drawable.ic_screen_rotation)
                .setOngoing(true)
                .setContentTitle(getString(R.string.notification_service_running))
                .setTicker(getString(R.string.notification_service_running))

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notificationChannel = NotificationChannel("persistent", getString(R.string.notification_persistent), NotificationManager.IMPORTANCE_NONE)
            notificationChannel.description = getString(R.string.notification_persistent_channel_description)
            val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(notificationChannel)
        }

        startForeground(FOREGROUND_ID, notificationBuilder.build())
    }

    companion object {

        private const val FOREGROUND_ID = 10001

        const val BATTERY_OPTIMIZATION_REQUEST_CODE = 20002

        fun startService(context: Context) {
            val prefs = Prefs.getInstance(context)
            if (prefs.isEnabled) {
                Timber.i("Starting Auto Auto-Rotate")
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O &&
                    shouldStartForegroundService(context)) {
                    Timber.d("Starting service with context.startForegroundService (Android >= Oreo and battery optimization on)")
                    context.startForegroundService(Intent(context, PersistentService::class.java))
                } else {
                    Timber.d("Starting service with context.startService (Android < Oreo or battery optimization off)")
                    context.startService(Intent(context, PersistentService::class.java))
                }
            } else {
                Timber.i("Not starting Auto Auto-Rotate because it's disabled")
            }
        }

        fun stopService(context: Context) {
            context.stopService(Intent(context, PersistentService::class.java))
        }

        fun restartService(context: Context) {
            Timber.i("Restarting Auto Auto-Rotate")
            stopService(context)
            startService(context)
        }

        private fun shouldStartForegroundService(context: Context): Boolean {
            return !context.isIgnoringBatteryOptimizations || Prefs.getInstance(context).showNotification
        }
    }
}
