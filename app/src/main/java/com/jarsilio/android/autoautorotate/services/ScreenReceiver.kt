package com.jarsilio.android.autoautorotate.services

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.provider.Settings
import com.jarsilio.android.autoautorotate.applist.AppDatabase
import com.jarsilio.android.autoautorotate.extensions.currentlyRunningApp
import java.util.Timer
import java.util.TimerTask
import timber.log.Timber

object ScreenReceiver : BroadcastReceiver() {

    private var timer: Timer = Timer()

    override fun onReceive(context: Context, intent: Intent) {

        if (intent.action == Intent.ACTION_SCREEN_OFF) {
            Timber.d("Screen off. Disabling scheduled task to check running app (TimerTask)")
            unscheduleTimer()
        } else if (intent.action == Intent.ACTION_SCREEN_ON) {
            Timber.d("Screen on. Re-enabling scheduled task to check running app (TimerTask)")
            scheduleTimer(context)
        }
    }

    private fun setAutoOrientation(context: Context, value: Boolean) {
        val currentValue = Settings.System.getInt(context.contentResolver, Settings.System.ACCELEROMETER_ROTATION) == 1
        if (value != currentValue) {
            Settings.System.putInt(context.contentResolver, Settings.System.ACCELEROMETER_ROTATION, if (value) 1 else 0)
        }
    }

    private fun unscheduleTimer() {
        timer.cancel()
    }

    fun scheduleTimer(context: Context) {

        var lastApp = ""

        timer.cancel()
        timer = Timer()
        timer.schedule(object : TimerTask() {

            override fun run() {
                val currentlyRunningApp = context.currentlyRunningApp

                if (currentlyRunningApp != lastApp) { // Only show the log once, and not every second
                    if (autoRotateAppListContainsCurrentlyRunningApp(context, currentlyRunningApp)) {
                        Timber.d("$currentlyRunningApp is in the list for auto-rotate. Setting auto-rotate to true (if not already set)")
                        setAutoOrientation(context, true)
                    } else {
                        Timber.d("$currentlyRunningApp is *not* in the list for auto-rotate. Setting auto-rotate to false (if not already set)")
                        setAutoOrientation(context, false)
                    }
                }
                lastApp = currentlyRunningApp
            }
        }, 500, 1000)
    }

    fun autoRotateAppListContainsCurrentlyRunningApp(context: Context, currentlyRunningApp: String): Boolean {
        for (app in AppDatabase.getInstance(context).appsDao().autorotateApps) {
            if (app.packageName == currentlyRunningApp) {
                Timber.d("${app.name} (${app.packageName}) is running in foreground")
                return true
            }
        }
        return false
    }
}
