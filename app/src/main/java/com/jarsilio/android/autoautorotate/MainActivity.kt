package com.jarsilio.android.autoautorotate

import android.content.Intent
import android.content.SharedPreferences
import android.content.res.Configuration
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.FrameLayout
import android.widget.ProgressBar
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import com.jarsilio.android.autoautorotate.appintro.AppIntro
import com.jarsilio.android.autoautorotate.applist.AppsHandler
import com.jarsilio.android.autoautorotate.extensions.isIgnoringBatteryOptimizations
import com.jarsilio.android.autoautorotate.extensions.isUsageAccessAllowed
import com.jarsilio.android.autoautorotate.extensions.isWriteSettingsAllowed
import com.jarsilio.android.autoautorotate.prefs.Prefs
import com.jarsilio.android.autoautorotate.services.PersistentService
import com.jarsilio.android.common.menu.CommonMenu
import com.jarsilio.android.common.privacypolicy.PrivacyPolicyBuilder
import com.mikepenz.aboutlibraries.Libs
import com.mikepenz.aboutlibraries.LibsBuilder
import timber.log.Timber

class MainActivity : AppCompatActivity() {

    private val prefs: Prefs by lazy { Prefs.getInstance(this) }
    private val commonMenu: CommonMenu by lazy { CommonMenu(this) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        val toolbar = findViewById<View>(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        supportActionBar?.setHomeButtonEnabled(true)
        supportFragmentManager.beginTransaction().replace(R.id.settings, SettingsFragment()).commit()

        AppsHandler(this).updateAppsDatabase()
    }

    override fun onStart() {
        super.onStart()

        if (!isUsageAccessAllowed || !isWriteSettingsAllowed) {
            Timber.d("Either 'Usage access' or 'Write settings' permission are not allowed. Disabling Auto Auto-Rotate and starting AppIntro")

            prefs.isEnabled = false
            PersistentService.stopService(this)

            startActivity(Intent(this, AppIntro::class.java))
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        commonMenu.addImpressumToMenu(menu)
        commonMenu.addSendDebugLogsToMenu(menu)

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menu_item_privacy_policy -> showPrivacyPolicyActivity()
            R.id.menu_item_licenses -> showAboutLicensesActivity()
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onResume() {
        super.onResume()
        invalidateOptionsMenu()
    }

    private fun showPrivacyPolicyActivity() {
        val privacyPolicyBuilder = PrivacyPolicyBuilder()
                .withIntro(getString(R.string.app_name), "Juan García Basilio (juanitobananas)")
                .withUrl("https://gitlab.com/juanitobananas/auto-auto-rotate/blob/master/PRIVACY.md#auto-auto-rotate-privacy-policy")
                .withMeSection()
                .withEmailSection("juam+auto-auto-rotate@posteo.net")
                .withAutoGoogleOrFDroidSection()
        privacyPolicyBuilder.start(this)
    }

    private fun showAboutLicensesActivity() {
        var style = Libs.ActivityStyle.LIGHT_DARK_TOOLBAR
        var theme = R.style.AppTheme_About_Light

        val currentNightMode = resources.configuration.uiMode and Configuration.UI_MODE_NIGHT_MASK
        if (currentNightMode == Configuration.UI_MODE_NIGHT_YES) {
            style = Libs.ActivityStyle.DARK
            theme = R.style.AppTheme_About_Dark
        }

        LibsBuilder()
                .withActivityStyle(style)
                .withActivityTheme(theme)
                .withAboutIconShown(true)
                .withAboutVersionShown(true)
                .withActivityTitle(getString(R.string.menu_item_licenses))
                .withAboutDescription(getString(R.string.licenses_about_libraries_text, getString(R.string.app_name)))
                .start(applicationContext)
    }

    class SettingsFragment : PreferenceFragmentCompat(), SharedPreferences.OnSharedPreferenceChangeListener {

        private val prefs by lazy { Prefs.getInstance(requireContext()) }
        private val progressBar: ProgressBar by lazy { requireActivity().findViewById(R.id.progressBar) }
        private val settingsLayout: FrameLayout by lazy { requireActivity().findViewById(R.id.settings) }

        override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
            setPreferencesFromResource(R.xml.root_preferences, rootKey)
        }

        override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
            super.onViewCreated(view, savedInstanceState)
            val appListPreference = findPreference<Preference>(prefs.APP_LIST)
            appListPreference?.setOnPreferenceClickListener {
                progressBar.visibility = View.VISIBLE // This progress bar is hidden onResume(). I would've liked to add this to AppListActivity while the RecyclerView was filling in, but I just couldn't manage. This seems to do the trick pretty well.
                settingsLayout.visibility = View.INVISIBLE
                startActivity(Intent(activity, AppListActivity::class.java))
                true
            }
        }

        override fun onStart() {
            super.onStart()
            prefs.settingsFragment = this
        }

        override fun onResume() {
            super.onResume()
            prefs.prefs.registerOnSharedPreferenceChangeListener(this)
            settingsLayout.visibility = View.VISIBLE
            progressBar.visibility = View.INVISIBLE
        }

        override fun onPause() {
            super.onPause()
            prefs.prefs.unregisterOnSharedPreferenceChangeListener(this)
        }

        override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences?, key: String?) {
            Timber.d("Changed preference: $key")
            when (key) {
                prefs.IS_ENABLED -> {
                    if (prefs.isEnabled) {
                        PersistentService.startService(requireContext())
                    } else {
                        PersistentService.stopService(requireContext())
                    }
                }
                prefs.SHOW_NOTIFICATION -> {
                    if (!prefs.showNotification && !requireContext().isIgnoringBatteryOptimizations) {
                        Timber.d("Requesting to ignore battery optimizations")
                        batteryOptimizationSettingsActivityResultLauncher.launch(Intent(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS,
                                Uri.parse("package:${requireContext().packageName}")))
                    } else {
                        PersistentService.restartService(requireContext())
                    }
                }
            }
        }

        private val batteryOptimizationSettingsActivityResultLauncher by lazy {
            registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
                if (!requireContext().isIgnoringBatteryOptimizations) {
                    Timber.d("The user didn't accept the ignoring of the battery optimization. Forcing show_notification to true")
                    prefs.showNotification = true
                } else {
                    PersistentService.restartService(requireContext())
                }
            }
        }
    }
}
