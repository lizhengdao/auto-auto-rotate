package com.jarsilio.android.autoautorotate

import android.app.Application
import android.content.Context
import androidx.appcompat.app.AppCompatDelegate
import com.jarsilio.android.common.logging.LongTagTree
import com.jarsilio.android.common.logging.PersistentTree
import org.acra.ACRA
import org.acra.annotation.AcraCore
import org.acra.annotation.AcraMailSender
import org.acra.annotation.AcraNotification
import timber.log.Timber

const val MAX_TAG_LENGTH = 23

@AcraCore(buildConfigClass = BuildConfig::class)
@AcraMailSender(mailTo = "juam+auto-auto-rotate@posteo.net")
@AcraNotification(
        resTitle = R.string.acra_notification_title,
        resText = R.string.acra_notification_text,
        resChannelName = R.string.acra_notification_channel_name,
        resSendButtonText = R.string.acra_notification_send,
        resDiscardButtonText = android.R.string.cancel,
        resSendButtonIcon = R.drawable.ic_email_gray,
        resDiscardButtonIcon = R.drawable.ic_cancel_gray
)

class App : Application() {
    override fun onCreate() {
        super.onCreate()
        Timber.plant(LongTagTree(this))
        Timber.plant(PersistentTree(this))
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM)
    }

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)

        if (!BuildConfig.DEBUG) {
            ACRA.init(this)
        }
    }
}
