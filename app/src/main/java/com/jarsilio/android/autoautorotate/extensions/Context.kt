package com.jarsilio.android.autoautorotate.extensions

import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.app.ActivityManager
import android.app.AppOpsManager
import android.app.usage.UsageStatsManager
import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import android.os.PowerManager
import android.provider.Settings
import com.jarsilio.android.common.extensions.isLollipopOrNewer
import com.jarsilio.android.common.extensions.isQOrNewer
import timber.log.Timber

var lastUsedApp: String = ""

val Context.currentlyRunningApp: String
    get() {
        var currentlyRunningApp = ""
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            @SuppressLint("WrongConstant")
            val usageStatsManager = getSystemService("usagestats") as UsageStatsManager?
            if (usageStatsManager == null) {
                Timber.e("Couldn't determine currently running app (failed to get UsageStatsManager).")
            } else {
                val now = System.currentTimeMillis()
                val usageStatsList = usageStatsManager.queryUsageStats(UsageStatsManager.INTERVAL_DAILY, now - 1000 * 1000, now)
                var lastTimeUsed: Long = 0
                for (usageStats in usageStatsList) {
                    if (usageStats.lastTimeUsed > lastTimeUsed) {
                        lastTimeUsed = usageStats.lastTimeUsed
                        if (usageStats.packageName != "android") { // I don't know what this is exactly, but sometimes this appid gets in the way.
                            currentlyRunningApp = usageStats.packageName
                        } else {
                            Timber.d("Couldn't determine currently running app (packagename 'android').")
                        }
                    }
                }
            }
        } else {
            val activityManager = getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
            val tasks = activityManager.runningAppProcesses
            if (tasks != null && tasks[0] != null) {
                if (tasks[0].processName != "android") {
                    currentlyRunningApp = tasks[0].processName
                } else {
                    Timber.d("Couldn't determine currently running app (packagename 'android').")
                }
            } else {
                Timber.e("Couldn't determine currently running app (using ActivityManager).")
            }
        }

        if (currentlyRunningApp == "") {
            Timber.d("Assuming it's still the last one: $lastUsedApp")
            currentlyRunningApp = lastUsedApp
        }

        lastUsedApp = currentlyRunningApp

        return currentlyRunningApp
    }

val Context.isScreenOn: Boolean
    get() {
        val powerManager = getSystemService(Context.POWER_SERVICE) as PowerManager

        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT_WATCH) {
            powerManager.isInteractive
        } else {
            powerManager.isScreenOn
        }
    }

val Context.isUsageAccessAllowed: Boolean

    @TargetApi(Build.VERSION_CODES.Q)
    get() {
        return if (isLollipopOrNewer) {
            try {
                val packageManager = applicationContext.packageManager
                val applicationInfo = packageManager.getApplicationInfo(applicationContext.packageName, 0)
                val appOpsManager = applicationContext.getSystemService(Context.APP_OPS_SERVICE) as AppOpsManager
                val mode = if (isQOrNewer) {
                    appOpsManager.unsafeCheckOpNoThrow(
                            AppOpsManager.OPSTR_GET_USAGE_STATS,
                            applicationInfo.uid,
                            applicationInfo.packageName)
                } else {
                    appOpsManager.checkOpNoThrow(
                            AppOpsManager.OPSTR_GET_USAGE_STATS,
                            applicationInfo.uid,
                            applicationInfo.packageName)
                }
                mode == AppOpsManager.MODE_ALLOWED
            } catch (e: PackageManager.NameNotFoundException) {
                false
            }
        } else {
            true
        }
    }

val Context.isWriteSettingsAllowed: Boolean
    get() = Build.VERSION.SDK_INT < Build.VERSION_CODES.M || Settings.System.canWrite(applicationContext)

val Context.isIgnoringBatteryOptimizations: Boolean
    get() {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val powerManager = applicationContext.getSystemService(Context.POWER_SERVICE) as PowerManager
            powerManager.isIgnoringBatteryOptimizations(applicationContext.packageName)
        } else {
            true
        }
    }
