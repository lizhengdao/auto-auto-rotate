New in version 0.6

★ Reproducible build: first attempt to make the apk build
  reproducible.

New in version 0.5

★ Add 'Legal notice' and 'Report an issue' menu items.
★ Some small improvements.

New in version 0.4

★ Some small fixes.

New in version 0.3

★ Add 'Auto-rotate apps' option to show the complete list of apps.
★ Add ACRA crash reporting.
★ Fix algorithm to detect last running app.
★ Minor fix: title had wrong colour in some activities.
