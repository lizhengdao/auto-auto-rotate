New in version 0.4

★ Some small fixes.

New in version 0.3

★ Add 'Auto-rotate apps' option to show the complete list of apps.
★ Add ACRA crash reporting.
★ Fix algorithm to detect last running app.
★ Minor fix: title had wrong colour in some activities.

New in version 0.2

★ Well, everything :) Very first version of Auto Auto-Rotate.
